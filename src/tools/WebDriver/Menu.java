package tools.WebDriver;

import org.openqa.selenium.Keys;

public class Menu
{
	//the first way
	/**
	 * @param menuXpath - main xpath of the menu
	 * @param menuLvlsLocators - xpaths for all menu levels
	 */
	public Menu(final String menuXpath, final String... menuLvlsLocators)
	{
		this.menuXpath = menuXpath;
		this.menuLvlsLocators = menuLvlsLocators;
	}
	
	public Menu open(final String... menuItems)
	{
		if ( menuItems.length > menuLvlsLocators.length || menuItems.length == 0) throw new RuntimeException("Not correct menu depth");
		
		String currentMenuItemXpath = this.menuXpath;
		
		if (menuItems.length > 1)
		{
			currentMenuItemXpath += String.format(menuLvlsLocators[0], menuItems[0]);
			HtmlElement theFirstMenuItem = new HtmlElement( currentMenuItemXpath );
			theFirstMenuItem.sendKeys(Keys.DOWN);

			for (int i = 1; i < menuItems.length - 1; i++)
			{
				currentMenuItemXpath += String.format(menuLvlsLocators[i], menuItems[i]);
				HtmlElement intermediateMenuItem = new HtmlElement( currentMenuItemXpath );
				intermediateMenuItem.sendKeys(Keys.RIGHT);
			}
		}
		
		currentMenuItemXpath += String.format(menuLvlsLocators[menuItems.length-1], menuItems[menuItems.length-1]);
		HtmlElement theLastMenuItem = new HtmlElement( currentMenuItemXpath );
		theLastMenuItem.sendKeys(Keys.ENTER);
		
		return this;
	}
	
	private String menuXpath;
	private String[] menuLvlsLocators;
	
	//the second way
	public Menu(final String menuItemXpath)
	{
		this.menuItemXpath = menuItemXpath;
	}
	
	public Menu down(final String menuItem)
	{
		HtmlElement menuItemEl = new HtmlElement( String.format(menuItemXpath, menuItem) );
		menuItemEl.sendKeys(Keys.DOWN);
		return this;
	}
	public Menu right(final String menuItem)
	{
		HtmlElement menuItemEl = new HtmlElement( String.format(menuItemXpath, menuItem) );
		menuItemEl.sendKeys(Keys.RIGHT);
		return this;
	}
	public Menu enter(final String menuItem)
	{
		HtmlElement menuItemEl = new HtmlElement( String.format(menuItemXpath, menuItem) );
		menuItemEl.sendKeys(Keys.ENTER);
		return this;
	}
	
	private String menuItemXpath;
	
}
