package tools.WebDriver;

import org.openqa.selenium.interactions.Actions;

public class HtmlElement
{
	
	protected String xpath;
	
	public HtmlElement(final String xpath)
	{
		this.xpath = xpath;
	}
	
	public String getXpath()
	{
		return xpath;
	}
	
	public String getText()
	{
		return WebDriverManager.Instance()
				.getElementByXpath(xpath).getText();
	}
	
	public boolean isPresent()
	{
		WebDriverManager wdManager = WebDriverManager.Instance();
		return wdManager.getElementsByXpath(xpath).size() > 0;
	}
	
	public boolean isDisplayed()
	{
		WebDriverManager wdManager = WebDriverManager.Instance();
		return wdManager.getElementByXpath(xpath).isDisplayed();
	}
	
	public boolean isVisible()
	{
		return isPresent() && isDisplayed();
	}
	
	public HtmlElement click()
	{
		WebDriverManager.Instance()
			.getElementByXpath(xpath)
			.click();
		return this;
	}
	
	public HtmlElement rightClick()
	{
		WebDriverManager wdm = WebDriverManager.Instance();
		new Actions(wdm.getDriver())
			.contextClick( wdm.getElementByXpath(xpath) )
			.build()
			.perform();
		return this;
	}
	
	/*public HtmlElement getChild(String childXpath)
	{
		return new HtmlElement(xpath + childXpath);
	}*/
	
	public HtmlElement sendKeys(final CharSequence... args)
	{
		WebDriverManager.Instance()
			.getElementByXpath(xpath)
			.sendKeys(args);
		return this;
	}
	
	public HtmlElement sendKeysToCursor(final CharSequence... args)
	{
		Actions act = WebDriverManager.Instance().actions();
		act.sendKeys(args);
		act.perform();
		return this;
	}
	
}
