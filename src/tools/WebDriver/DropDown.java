package tools.WebDriver;

public class DropDown extends HtmlElement
{
	
	public DropDown(final String xpath)
	{
		super(xpath);
	}
	
	public DropDown(final String xpath, final String optionsXpath)
	{
		this(xpath);
		this.optionsXpath = optionsXpath;
	}
	
	public DropDown select(final String option)
	{
		new HtmlElement(xpath + String.format(optionsXpath, option)).click();
		return this;
	}
	
	private String optionsXpath = "//option[text()='%s']";

}
