package tools.WebDriver;

public class TextBox extends HtmlElement
{
	
	public TextBox(final String xpath)
	{
		super(xpath);
	}
	
	public TextBox input(final String text)
	{
		clear();
		sendKeys(text);
		return this;
	}
	
	public TextBox sendKeys(final String text)
	{
		super.sendKeys(text);
		return this;
	}
	
	public TextBox clear()
	{
		WebDriverManager.Instance()
			.getElementByXpath(xpath)
			.clear();
		return this;
	}
	
	public String getValue()
	{
		return WebDriverManager.Instance()
				.getElementByXpath(xpath)
				.getAttribute("value");
	}

}
