package tools.WebDriver;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeoutException;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tools.TimeOuts;

public class WebDriverManager
{
	
	private static WebDriverManager instance = null;
	private WebDriverManager() {}
	
	public static WebDriverManager Instance()
	{
		if ( instance == null)
		{
			WebDriverManager.instance = new WebDriverManager();
		}
		return instance;
	}
	
	//was opened for actions
	//TODO: add work with Actions to WebDriverManager and delete this get
	public WebDriver getDriver()
	{
		return driver;
	}
	
	public WebDriverManager start(WebDriverManager.Browsers browser)
	{
		System.setProperty("webdriver.ie.driver","resources/drivers/IEDriverServer.exe");
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		InternetExplorerOptions ieOptions = new InternetExplorerOptions(capabilities);
		driver = new InternetExplorerDriver(ieOptions);
		
		return this;
	}
	
	public WebDriverManager stop()
	{
		driver.quit();
		return this;
	}
	
	public WebDriverManager close()
	{
		driver.close();
		return this;
	}
	
	public WebDriverManager open(String url)
	{
		driver.navigate().to(url);
		return this;
	}
	
	public WebElement getElementByXpath(final String xpath)
	{
		return driver.findElement( By.xpath(xpath) );
	}
	
	public List<WebElement> getElementsByXpath(final String xpath)
	{
		return driver.findElements( By.xpath(xpath) );
	}
	
	//switch
	public WebDriverManager switchToWindow(final String title)
	{
		driver.getWindowHandles().stream()
			.filter(handler -> driver.switchTo().window(handler).getTitle().contains(title))
			.findFirst();
		return this;
	}
	
	public WebDriverManager switchToIFrame(int num)
	{
		WebElement iframe = driver.findElements(By.xpath("//iframe")).get(num);
		driver.switchTo().frame(iframe);
		return this;
	}
	public Alert switchToAllert()
	{
		return driver.switchTo().alert();
	}
	
	//waits
	public WebDriverManager wait(ExpectedCondition<?> condition)
	{
		return wait(condition, DEFAULT_WAIT);
	}
	public WebDriverManager wait(ExpectedCondition<?> condition, long timeToWait)
	{
		new WebDriverWait(driver, timeToWait).until(condition);
		return this;
	}
	
	public WebDriverManager waitForWindowToBeAvailableAndSwitchToIt(String title) throws TimeoutException
	{
		long startTime = new Date().getTime();
		while ( !doesWindowExist(title) && ( (new Date().getTime() - startTime) < NEW_WINDOW_OPEN_TIMEOUT ) )
		{
			try {
				Thread.sleep(100);
			} catch (InterruptedException e)
			{
				throw new RuntimeException(e);
			}
		}
		if ( !doesWindowExist(title) )
		{
			throw new TimeoutException( String.format("The window '%s' did not open during '%d'ms", title, NEW_WINDOW_OPEN_TIMEOUT) );
		}
		
		return this;
	}
	public boolean doesWindowExist(String title)
	{
		return driver.getWindowHandles().stream()
				.anyMatch(handler -> driver.switchTo().window(handler).getTitle().contains(title));
	}
	
	public WebDriverManager waitForPageLoad()
	{
		try
		{
			Thread.sleep(300);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		new WebDriverWait(driver, PAGE_LOAD_TIMEOUT)
			.until(
					webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete")
					);
		
		return this;
	}
	
	public WebDriverManager waitForElementDisappear(final HtmlElement el)
	{
		return waitForElementDisappear(el.getXpath());
	}
	public WebDriverManager waitForElementDisappear(final String xpath)
	{
		return waitForElementDisappear(xpath, PROCESSING_WAIT_TIMEOUT);
	}
	public WebDriverManager waitForElementDisappear(final HtmlElement el, long timeToWait)
	{
		return waitForElementDisappear(el.getXpath(), timeToWait);
	}
	public WebDriverManager waitForElementDisappear(final String xpath, long timeToWait)
	{
		new WebDriverWait(driver, timeToWait)
			.until( ExpectedConditions.invisibilityOf(getElementByXpath(xpath)) );
		return this;
	}
	
	public Actions actions()
	{
		return new Actions(driver);
	}
	
	/*public static class Actions
	{
		public static void rigthClick()
		{
			
		}
	}*/
	
	private WebDriver driver;
	private final long DEFAULT_WAIT = TimeOuts.THIRTY_SECONDS.inSec();
	private final long PAGE_LOAD_TIMEOUT = 90;//TimeOuts.ONE_MIN.inSec();
	private final long PROCESSING_WAIT_TIMEOUT = TimeOuts.THREE_SECONDS.inSec();
	private final long NEW_WINDOW_OPEN_TIMEOUT = TimeOuts.THREE_SECONDS.inMillSec();
	
	public static enum Browsers
	{
		IE,
		CHROME
	}

}
