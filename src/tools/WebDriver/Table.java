package tools.WebDriver;

public class Table
{
	
	public Table(final String tableXpath, final String headersXpath, final String rowsXpath)
	{
		this.tableXpath = tableXpath;
		this.headersXpath = headersXpath;
		this.rowsXpath = rowsXpath;
	}
	
	protected String tableXpath;
	protected String headersXpath;
	protected String rowsXpath;

}
