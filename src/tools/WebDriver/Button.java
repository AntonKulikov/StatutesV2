package tools.WebDriver;

public class Button extends HtmlElement
{
	
	public Button(final String xpath)
	{
		super(xpath);
	}
	
	public Button click()
	{
		super.click();
		return this;
	}

}
