package tools.WebDriver;

import org.openqa.selenium.support.ui.ExpectedConditions;

public class Alert
{
	
	public static void accept()
	{
		waitIsPresent();
		WebDriverManager.Instance()
			.switchToAllert()
			.accept();
	}
	
	public static void waitIsPresent()
	{
		WebDriverManager.Instance()
			.wait(ExpectedConditions.alertIsPresent());
	}

}
