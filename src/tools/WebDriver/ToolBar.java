package tools.WebDriver;

import java.util.HashMap;
import java.util.Map;

public abstract class ToolBar
{
	/*public ToolBar()
	{
		
	}*/
	
	protected void click(TB_Els buttonName)
	{
		buttons.get(buttonName).click();
	}
	
	protected Map<TB_Els, Button> buttons = new HashMap<>();
	
	protected interface TB_Els{}

}
