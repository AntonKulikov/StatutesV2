package tools;

public enum TimeOuts
{
	ONE_MIN(60000),
	THIRTY_SECONDS(30000),
	THREE_SECONDS(3000)
	;
	
	private TimeOuts(long milliseconds)
	{
		this.milliseconds = milliseconds;
	}
	
	public long inMillSec()
	{
		return milliseconds;
	}
	
	public long inSec()
	{
		return milliseconds/1000;
	}
	
	private long milliseconds;

}
