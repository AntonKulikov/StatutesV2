package tests.editor;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import models.services.EditorTestingService;
import models.services.HierarchyTestingService;
import models.services.NavigatorTestingService;
import tests.BaseTest;
import tools.WebDriver.HtmlElement;


public class AddedDeletedToolbarTagsLegalTests extends BaseTest
{
	@Test//только начал:)
	public void addTagLegalTest()
    {
    	String phrase = "TESTING ADD";
    	setUpNoLogger(Users.CODESBENCH);
    	
    	NavigatorTestingService.goToHierarchyNavigate();
    	HierarchyTestingService.quickSearch("12.14");
    	HierarchyTestingService.editSelectedSiblingMetadata();
    	
    	EditorTestingService.switchToEditor();
		EditorTestingService.switchToTextFrame();
		
    	EditorTestingService.textArea.insertAndHighlightPhrase(phrase, "//para/span");
    	EditorTestingService.switchToEditor();
    	EditorTestingService.toolBar.addMarkup();
    	EditorTestingService.switchToTextFrame();
    	
    	boolean addTagsAppeared = new HtmlElement("//paratext//added.material[text()='" + phrase + "']").isVisible();
    	
    	assertTrue(addTagsAppeared);
    	//EditorTestingService.closeWithNoChanges();
    	
    	/*
    	boolean addTagsAppear = editorTestingService.verifyAddedTagsAppearForPhrase(phrase);
    	Assert.assertTrue(addTagsAppear, "The added tags appeared");*/
    }

}
