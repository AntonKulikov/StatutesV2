package tests.editor;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.TimeoutException;

import org.junit.jupiter.api.Test;

import models.services.EditorTestingService;
import models.services.HierarchyTestingService;
import models.services.NavigatorTestingService;
import tests.BaseTest;
import tools.WebDriver.Alert;
import tools.WebDriver.WebDriverManager;

public class EditHVSInfoTests extends BaseTest
{
	
	@Test
	public void test1() throws TimeoutException
	{
		String newSerialNumber = "1965118191";
		String newReporterVolume = "133";
		String newReporterNumber = "595";
		String newReporterPage = "687";
		
		setUpNoLogger(Users.CODESBENCH);
		
		NavigatorTestingService.goToHierarchyNavigate();
		HierarchyTestingService.quickSearch("BLUE LINE", "550");
		HierarchyTestingService.editSelectedSiblingMetadata();
		
		EditorTestingService.switchToEditor();
		EditorTestingService.switchToTextFrame();
		
		String serialNumber = EditorTestingService.textArea.nod.note(1).getSerialNumber();
		String reporterVolume = EditorTestingService.textArea.nod.note(1).getReporterVolume();
		String reporterNumber = EditorTestingService.textArea.nod.note(1).getReporterNumber();
		String reporterPage = EditorTestingService.textArea.nod.note(1).getReporterPage();
		String headnoteNumber = EditorTestingService.textArea.nod.note(1).getHeadnoteNumber();
		
		EditorTestingService.textArea.nod.note(1).serialNumber.rightClick();
		EditorTestingService.switchToEditHVSInformation();
		
		String serialNumberHVS = EditorTestingService.editHVSInformation().serialNumber.getValue();
		String headnoteNumberHVS = EditorTestingService.editHVSInformation().headnoteNumber.getValue();
		String reporterVolumeHVS = EditorTestingService.editHVSInformation().reporterVolume.getValue();
		String reporterNumberHVS = EditorTestingService.editHVSInformation().reporterNumber.getValue();
		String reporterPageHVS = EditorTestingService.editHVSInformation().reporterPage.getValue();
		
		boolean serialNumberMatches = serialNumber.equals(serialNumberHVS);
    	boolean headnoteNumberMatches = headnoteNumber.equals(headnoteNumberHVS);
    	boolean reporterVolumeMatches = reporterVolume.equals(reporterVolumeHVS);
    	boolean reporterNumberMatches = reporterNumber.equals(reporterNumberHVS);
    	boolean reporterPageMatches = reporterPage.equals(reporterPageHVS);
		
    	EditorTestingService.editHVSInformation().serialNumber.input(newSerialNumber);
    	
    	EditorTestingService.editHVSInformation().suggest.click();
    	try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	Alert.accept();
    	WebDriverManager.Instance().waitForPageLoad();
    	
    	String serialNumberHVS2 = EditorTestingService.editHVSInformation().serialNumber.getValue();
    	String reporterVolumeHVS2 = EditorTestingService.editHVSInformation().reporterVolume.getValue();
    	String reporterNumberHVS2 = EditorTestingService.editHVSInformation().reporterNumber.getValue();
    	String reporterPageHVS2 = EditorTestingService.editHVSInformation().reporterPage.getValue();
    	
    	boolean serialNumberMatchesSwap = serialNumberHVS2.equals(newSerialNumber);
    	boolean reporterVolumeMatchesSwap = reporterVolumeHVS2.equals(newReporterVolume);
    	boolean reporterNumberMatchesSwap = reporterNumberHVS2.equals(newReporterNumber);
    	boolean reporterPageMatchesSwap = reporterPageHVS2.equals(newReporterPage);
    			
    	assertAll("Check before changes", () -> {
    		assertTrue(serialNumberMatches, "The serial number from the Edit HVS Info window matches the editor");           
    		assertTrue(headnoteNumberMatches, "The headnote number from the Edit HVS Info window matches the editor");       
    		assertTrue(reporterVolumeMatches, "The reporter volume number from the Edit HVS Info window matches the editor");
    		assertTrue(reporterNumberMatches, "The reporter number from the Edit HVS Info window matches the editor");       
    		assertTrue(reporterPageMatches, "The reporter page from the Edit HVS Info window matches the editor");           
    	});
    	
    	assertAll("Check after changes", () -> {
    		assertTrue(serialNumberMatchesSwap, "The serial number updated appropriately after swapping the value");                   
    		assertTrue(reporterVolumeMatchesSwap, "The reporter volume number updated appropriately after swapping the serial number");
    		assertTrue(reporterNumberMatchesSwap, "The reporter number updated appropriately after swapping the serial number");       
    		assertTrue(reporterPageMatchesSwap, "The reporter page updated appropriately after swapping the serial number");           
    	});
    	
		
	}
	
	//@Test
	public void test2()
	{
		
		setUpNoLogger(Users.CODESBENCH);
		
		NavigatorTestingService.goToContentSets();
		
	}
	
}
