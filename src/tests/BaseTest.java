package tests;

import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import models.pageObjects.Login;
import tools.WebDriver.WebDriverManager;

public class BaseTest 
{
	
	@BeforeEach
	public void startDriver()
	{
		WebDriverManager.Instance().start(WebDriverManager.Browsers.IE);
	}
	
	@AfterEach
	public void stopDriver()
	{
		
		try
		{
			Thread.sleep(10000);
		} catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebDriverManager.Instance().stop();
		
		
	}
	
	protected void setUpNoLogger(Users user)
	{
		String userId, pass;
		switch(user)
		{
			case CODESBENCH:
			default:
				userId = "g086295";
				pass = "zipp3L#9";
				break;
		}
		
		WebDriverManager wdManager = WebDriverManager.Instance();
		wdManager.open(baseUrl);
		
		Login loginPO = new Login();
		if ( !loginPO.isOpen() ) Assert.fail("Login page should be open");
		loginPO
			.enterUserID(userId)
			.enterPassword(pass)
			.login();
		
		wdManager.waitForPageLoad();
	}
	
	final private String baseUrl = "http://uat.magellan3.int.westgroup.com:9183/codesBenchNavigatorWeb";
	
	public enum Users
	{
		CODESBENCH
	}

}
