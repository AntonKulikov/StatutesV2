package models.pageObjects;


import models.elements.LoginElements;

public class Login
{
	
	private LoginElements l_els = new LoginElements();
	
	public Login() {}
	
	public boolean isOpen()
	{
		return l_els.title.isVisible();
	}
	
	public Login enterUserID(final String login)
	{
		l_els.userId.input(login);
		return this;
	}
	
	public Login enterPassword(final String password)
	{
		l_els.password.input(password);
		return this;
	}
	
	public Login login()
	{
		l_els.login.click();
		return this;
	}
	
}
