package models.pageObjects;

import tools.WebDriver.Button;
import tools.WebDriver.TextBox;

public class EditHVSInformation
{
	
	
	final public TextBox serialNumber = new TextBox("//input[@id='pageForm:serialNumber']");
	final public TextBox headnoteNumber = new TextBox("//input[@id='pageForm:headnoteNumber']");
	final public TextBox reporterVolume = new TextBox("//input[@id='pageForm:reporterVolume']");
	final public TextBox reporterNumber = new TextBox("//input[@id='pageForm:reporterNumber']");
	final public TextBox reporterPage = new TextBox("//input[@id='pageForm:reporterPage']");
	
	final public Button suggest = new Button("//input[@id='pageForm:suggestButton']");
	
	/*
	String serialNumberHVS = baseTestingService.getElementByXPath("//input[@id='pageForm:serialNumber']").getAttribute("value");
	String headnoteNumberHVS = baseTestingService.getElementByXPath("//input[@id='pageForm:headnoteNumber']").getAttribute("value");
	String reporterVolumeHVS = baseTestingService.getElementByXPath("//input[@id='pageForm:reporterVolume']").getAttribute("value");
	String reporterNumberHVS = baseTestingService.getElementByXPath("//input[@id='pageForm:reporterNumber']").getAttribute("value");
	String reporterPageHVS = baseTestingService.getElementByXPath("//input[@id='pageForm:reporterPage']").getAttribute("value");*/

}
