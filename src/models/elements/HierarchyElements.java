package models.elements;

import tools.WebDriver.Button;
import tools.WebDriver.DropDown;
import tools.WebDriver.Menu;
import tools.WebDriver.TextBox;

public class HierarchyElements
{
	
	//quickSearch tab
	final public DropDown quickSearch_keyWord = new DropDown("//*[@id='topPageForm:keywordInput']");
	final public TextBox quickSearch_value = new TextBox("//*[@id='topPageForm:valueInput']");
	final public Button quickSearch_find = new Button("//*[@id='topPageForm:findQuickButton']");
		
	//Sibling Metadata
	final public SiblingMetadataTable siblingMetadataTable = new SiblingMetadataTable(
			"//*[@id='siblingMetadataGrid']/table", "//thead//th", "//tbody//tr"
			);
		
	final public Menu siblingMetadataContextMenu = new Menu(
			"//*[@id='siblingMetadataContextMenu']",
			"//a[text()='%s']",
			"/ancestor::li//a[text()='%s']"
			);

}
