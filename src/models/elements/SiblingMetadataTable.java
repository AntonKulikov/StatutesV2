package models.elements;

import tools.WebDriver.HtmlElement;
import tools.WebDriver.Table;

public class SiblingMetadataTable extends Table
{
	
	public SiblingMetadataTable(final String tableXpath, final String headersXpath, final String rowsXpath)
	{
		super(tableXpath, headersXpath, rowsXpath);
	}
	
	public HtmlElement getSelectedRow()
	{
		return new HtmlElement(tableXpath + rowsXpath + SELECTED);
	}
	
	final private static String SELECTED = "[contains(@class, 'selected')]";

}
