package models.elements.editor;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import models.pageObjects.EditHVSInformation;
import tools.WebDriver.HtmlElement;
import tools.WebDriver.WebDriverManager;

public class EditorTextArea extends HtmlElement
{
	public EditorTextArea(final String xpath)
	{
		super(xpath);
	}
	
	public EditorTextArea insertAndHighlightPhrase(final String phrase, final String xpath)
	{
		return insertPhrase(phrase, xpath)
				.highlightPhrase(phrase);
	}
	
	public EditorTextArea insertPhrase(String phrase, String xpath)
    {
    	HtmlElement textLabel = new HtmlElement(xpath);
    	textLabel.click();
    	textLabel.sendKeys(Keys.ARROW_DOWN);
    	textLabel.sendKeys(Keys.HOME);
    	//insert the phrase as well as a space at the end of it and then move the cursor left one
    	
    	sendKeysToCursor(phrase);
		
		Actions cursor = WebDriverManager.Instance().actions();
    	cursor.sendKeys(Keys.SPACE).sendKeys(Keys.LEFT);
    	Action action = cursor.build();
        action.perform();
        
        return this;
    }
	
	public EditorTextArea highlightPhrase(String phrase)
    {
    	Actions highlighter = WebDriverManager.Instance().actions();
        highlighter.keyDown(Keys.SHIFT);

        for (int i = 0; i < phrase.length(); i++)
        {
            highlighter.sendKeys(Keys.LEFT);
        }

        highlighter.keyUp(Keys.SHIFT);
        Action action = highlighter.build();
        action.perform();
        
        return this;
    }
	
	public EditorNOD nod = new EditorNOD("//nod.body");
	public EditHVSInformation editHVSInformation = new EditHVSInformation();

}
