package models.elements.editor;

import tools.WebDriver.HtmlElement;

public class EditorNOD extends HtmlElement
{
	public EditorNOD(String xpath)
	{
		super(xpath);
	}
	
	public EditorNote note(int num)
	{
		return new EditorNote("(" + this.xpath + NOTES_XPATH + ")" + "[" + num + "]");
	}
	
	final private String NOTES_XPATH = "//nod.note";
}
