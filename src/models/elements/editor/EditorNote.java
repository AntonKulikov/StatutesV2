package models.elements.editor;

import tools.WebDriver.HtmlElement;

public class EditorNote extends HtmlElement
{
	public EditorNote(String xpath)
	{
		super(xpath);
	}
	
	public String getSerialNumber()
	{
		String[] str = serialNumber.getText().trim().split(" ");
		return str[str.length-1];
	}
	
	public String getReporterVolume()
	{
		String[] str = reporterVolume.getText().trim().split(" ");
		return str[str.length-1];
	}
	
	public String getReporterNumber()
	{
		String[] str = reporterNumber.getText().trim().split(" ");
		return str[str.length-1];
	}
	
	public String getReporterPage()
	{
		String[] str = reporterPage.getText().trim().split(" ");
		return str[str.length-1];
	}
	
	public String getHeadnoteNumber()
	{
		String[] str = headnoteNumber.getText().trim().split(" ");
		return str[str.length-1];
	}
	
	final public HtmlElement serialNumber = new HtmlElement(this.xpath + "//metadata.block//md.serial.no");
	final public HtmlElement reporterVolume = new HtmlElement(this.xpath + "//metadata.block//md.reporter.volume");
	final public HtmlElement reporterNumber = new HtmlElement(this.xpath + "//metadata.block//md.reporter.number");
	final public HtmlElement reporterPage = new HtmlElement(this.xpath + "//metadata.block//md.reporter.page");
	final public HtmlElement headnoteNumber = new HtmlElement(this.xpath + "//metadata.block//md.headnote.no");
	
}
