package models.elements.editor;

import tools.WebDriver.Button;
import tools.WebDriver.ToolBar;

public class EditorToolBar extends ToolBar 
{
	
	public EditorToolBar()
	{
		this.buttons.put( ETB_Els.CLOSE_DOC, new Button("//div[@class='toolbar']//a[@title='Close Doc']") );
		this.buttons.put( ETB_Els.ADD, new Button("//div[@class='toolbar']//a[@title='Add']"));
	}
	
	public EditorToolBar closeDoc()
	{
		this.click(ETB_Els.CLOSE_DOC);
		return this;
	}
	
	public EditorToolBar addMarkup()
	{
		this.click(ETB_Els.ADD);
		return this;
	}
	
	private enum ETB_Els implements TB_Els
	{
		CLOSE_DOC,
		ADD
	}

	
}
