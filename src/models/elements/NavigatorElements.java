package models.elements;

import tools.WebDriver.Menu;

public class NavigatorElements
{
	
	final public Menu codesNavigatorMenu = new Menu(
													"//*[@id='codesNavigatorMenu']",
													"//a[text()='%s']",
													"/ancestor::li//a[text()='%s']",
													"/ancestor::li//a[text()='%s']"
												   );
	
	//just for example
	final public Menu codesNavigatorMenu2 = new Menu("//*[@id='codesNavigatorMenu']//a[text()='%s']");	

}
