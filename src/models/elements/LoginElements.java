package models.elements;

import tools.WebDriver.Button;
import tools.WebDriver.HtmlElement;
import tools.WebDriver.TextBox;

public class LoginElements
{
	
	final public HtmlElement title = new HtmlElement("//div[@id='header']/h1[text()='Codes – Single Sign On (codesSSO)']");
	final public TextBox userId = new TextBox("//*[@id='username']");
	final public TextBox password = new TextBox("//*[@id='password']");
	final public Button login = new Button("//div[@id='login']//input[@name='submit']");

}
