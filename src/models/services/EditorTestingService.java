package models.services;

import java.util.concurrent.TimeoutException;

import models.elements.editor.EditorTextArea;
import models.elements.editor.EditorToolBar;
import models.elements.editor.EditorTree;
import models.pageObjects.EditHVSInformation;
import tools.WebDriver.WebDriverManager;

public class EditorTestingService
{
	private static String title = "Common Editor";
	
	public static boolean switchToEditor()
	{
		WebDriverManager.Instance()
			.switchToWindow(title)
			.waitForPageLoad();
		return true;//TODO: add check
	}
	
	public static boolean switchToEditHVSInformation() throws TimeoutException
	{
		WebDriverManager.Instance()
			.waitForWindowToBeAvailableAndSwitchToIt("Edit HVS information")
			.waitForPageLoad();
		return true;//TODO: add check
	}
	
	
	public static boolean switchToTextFrame()
	{
		WebDriverManager.Instance()
			.switchToIFrame(1);
		return true;//TODO: add check
	}
	
	public static void closeWithNoChanges()
    {
		toolBar.closeDoc();
    }
	
	
	public static EditHVSInformation editHVSInformation()
	{
		return textArea.editHVSInformation;
	}
	
	final public static EditorToolBar toolBar = new EditorToolBar();
	final public static EditorTextArea textArea = new EditorTextArea("");
	final public static EditorTree tree = new EditorTree();
	
	//private static EditorNOD nod = new EditorNOD("//nod.body");
	//private static EditHVSInformation editHVSInformation = new EditHVSInformation();
	
}
