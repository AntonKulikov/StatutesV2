package models.services;

import models.elements.NavigatorElements;
import tools.WebDriver.WebDriverManager;

public class NavigatorTestingService
{
	
	public static NavigatorElements n_els = new NavigatorElements();
	
	public static void goToHierarchyNavigate()
	{
		n_els.codesNavigatorMenu.open("Hierarchy", "Navigate");
		WebDriverManager.Instance().waitForPageLoad();
		//codesNavigatorMenu2
		//	.down("Hierarchy")
		//	.enter("Navigate");
		
	}
	
	public static void goToContentSets()
	{
		//codesNavigatorMenu.open("Home", "User Preferences", "Content Sets");
		n_els.codesNavigatorMenu2
			.down("Home")
			.right("User Preferences")
			.enter("Content Sets");
	}
	

}
