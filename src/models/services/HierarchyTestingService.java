package models.services;

import models.elements.CommonElements;
import models.elements.HierarchyElements;
import tools.WebDriver.WebDriverManager;

public class HierarchyTestingService
{
	private static HierarchyElements h_els = new HierarchyElements(); 
	
	public static void quickSearch(final String searchTerm)
    {
		h_els.quickSearch_value.input(searchTerm);
		h_els.quickSearch_find.click();
		WebDriverManager.Instance().waitForElementDisappear(CommonElements.PROCESSING_WAIT);
    }
	
	public static void quickSearch(final String keyword, final String searchTerm)
    {
		h_els.quickSearch_keyWord.select(keyword);
		quickSearch(searchTerm);
    }
	
	public static void editSelectedSiblingMetadata()
    {
		
		h_els.siblingMetadataTable.getSelectedRow().rightClick();
		h_els.siblingMetadataContextMenu.open("Edit Content");
		WebDriverManager.Instance().waitForPageLoad();
		
		EditorTestingService.switchToEditor();
		
    	/*rightClickSelectedSiblingMetadata();
    	openContextMenu(HierarchyContextMenuPOM.EDIT_CONTENT);
    	waitForPageLoaded();
    	switchToEditor();
        waitForPageLoaded();
        switchToEditorTextFrame();*/
    }

}
